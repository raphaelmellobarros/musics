# About the Project

## Install Dependencies
1. run "composer install" in root directory

## Database File
1. config/schema/database.sql

## User Credentials
1. Login:    "admin"
2. Password: "admin"

## See In
1. URL: http://apiservices.com.br/musics

# Author
1. @name:  Raphael de Mello Barros
2. @email: barros.raphael@yahoo.com.br

