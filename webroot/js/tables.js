$(function()
{
    $('.table-image-preview').on('click' , function()
    {
        var src= $( this ).attr( 'src' );

        $('#primaryModal .modal-title').text( 'Preview de Imagem');
        $('#primaryModal .modal-body').html( '<img class="img-responsive" style="width:100%;" src="' + src + '" alt="preview" />' );

        $('#primaryModal').modal();
    });
});