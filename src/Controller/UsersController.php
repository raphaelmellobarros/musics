<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\I18n\Number;
use Cake\ORM\TableRegistry;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 *
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class UsersController extends AppController
{

    public function initialize()
    {
        parent::initialize();

        $this->paginate = [
            'contain' => ['Artists', 'Albums' ]
        ];

        $this->Auth->allow();
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {

        $users = $this->paginate($this->Users);

        $this->set(compact('users'));
    }

    /**
     * View method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => ['Artists', 'Albums' ]
        ]);

        $this->set('user', $user);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $user = $this->Users->newEntity();
        if ($this->request->is('post')) {
            $user = $this->Users->patchEntity($user, $this->request->getData());
            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The user could not be saved. Please, try again.'));
        }
        $Artists = $this->Users->Artists->find('list', ['limit' => 200]);
        $Albums = $this->Users->Albums->find('list', ['limit' => 200]);
        $this->set(compact('user', 'Artists', 'Albums'));
    }

    /**
     * Edit method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => ['Albums']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $user = $this->Users->patchEntity($user, $this->request->getData());
            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The user could not be saved. Please, try again.'));
        }
        $Artists = $this->Users->Artists->find('list', ['limit' => 200]);
        $Albums = $this->Users->Albums->find('list', ['limit' => 200]);
        $this->set(compact('user', 'Artists', 'Albums'));
    }

    /**
     * Delete method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $user = $this->Users->get($id);
        if ($this->Users->delete($user)) {
            $this->Flash->success(__('The user has been deleted.'));
        } else {
            $this->Flash->error(__('The user could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function login()
    {
        $this->viewBuilder()->setLayout("login") ;

        $status = false;
        $data   = null;
        $message= __('Sorry, we couldn\'t find an account with this username. Please check you\'re using the right username and try again.');

        $user = $this->Users->newEntity();

        if ( $this->request->is( ['post'] ) )
        {
            $user = $this->Auth->identify();

            if ( !empty( $user ) )
            {
                if( $user['active'] == 'true' )
                {
                    $this->Auth->setUser( $user );

                    $status   = true;
                    //$data     = $this->Users->find( 'all' )->where( [ 'id' => $user['id'] ])->first() ;
                    $data       = $this->Users->get( $user['id'] , $this->paginate );
                    $message= __('Login successfully!');
                }
                else
                    $message = __('Your access is inactive!');
            }
            else
                $message= __('Sorry, we couldn\'t find an account with this username. Please check you\'re using the right username and try again.');
        }

        $response = $this->response( $status , $data , $message );

        $this->set(compact('response' , 'user'));
        $this->set('_serialize', 'response');
    }

    public function logout()
    {
        return $this->redirect( $this->Auth->logout() );
    }

    public function isLogged()
    {
        $status = !empty( $this->Auth->user() ) ;
        $data   = $status == true ? $this->Auth->user() : null;

        $removes = ['api_hash' , 'created' , 'modified' ];

        foreach( $removes as $remove )
            unset( $data[ $remove ] );


        $status = $status == true AND $data[ 'active'] == 'true';

        $message = $status == true ? __('User logged in!') : __( 'User is offline.');

        $response = $this->response($status , $data , $message );
        $isLogged = $status;

        $this->set(compact('response' , 'isLogged' ));
        $this->set('_serialize', 'response');
    }

    public function dashboard()
    {

        $user = $this->Auth->user();

        $albums         = TableRegistry::get( 'Albums' );
        $users          = TableRegistry::get( 'Users' );
        $artists        = TableRegistry::get( 'Artists' );

        $dashboards = null;

        //$dashboards['users']   = [ 'count' => $users->find('all')->count()  , 'color' => 'bg-danger' , 'title' => __( 'Usuários' ) , 'icon' => 'fa fa-user'];
        $dashboards['artists'] = [ 'count' => $artists->find('all')->count()  , 'color' => 'bg-success' , 'title' => __( 'Artists' ) , 'icon' => 'fa fa-users'];
        $dashboards['albums']  = [ 'count' => $albums->find('all')->count()  , 'color' => 'bg-info' , 'title' => __( 'Albums' ) , 'icon' => 'fa fa-book'];

        foreach( $dashboards as $qtd => $q )
            $dashboards[ $qtd ]['count'] = Number::format( $dashboards[ $qtd ]['count'] , [ 'places' => 0 , 'locale' => 'pt_BR' ] );

        $this->set( compact( 'dashboards') );
    }
}
