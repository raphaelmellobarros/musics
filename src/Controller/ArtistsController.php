<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Http\Exception\NotFoundException;

/**
 * Artists Controller
 *
 * @property \App\Model\Table\ArtistsTable $Artists
 *
 * @method \App\Model\Entity\Artist[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ArtistsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->breadcrumbs[ __('Artists' ) ] = ['controller' => 'Artists' , 'action' => 'index' ];

        $this->paginate = [
            'contain' => ['Users']
        ];
        $artists = $this->paginate($this->Artists);

        $this->set(compact('artists'));
    }

    /**
     * View method
     *
     * @param string|null $id Artist id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $this->breadcrumbs[ __('Artists' ) ] = ['controller' => 'Artists' , 'action' => 'index' ];

        $artist = $this->Artists->get($id, [
            'contain' => ['Users', 'Albums']
        ]);

        $this->set('artist', $artist);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $this->breadcrumbs[ __('Artists' ) ] = ['controller' => 'Artists' , 'action' => 'index' ];

        $artist = $this->Artists->newEntity();
        if ($this->request->is('post')) {
            $artist = $this->Artists->patchEntity($artist, $this->request->getData());

            $artist->set( 'user_id' , $this->Auth->user()['id'] );

            if ($this->Artists->save($artist)) {
                $this->Flash->success(__('The artist has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The artist could not be saved. Please, try again.'));
        }


        $this->set(compact('artist'));
    }

    /**
     * Método para procurar artistas por nome ou pelo twitter_handle
     */
    public function search()
    {
        $data   = null;
        $status = false;
        $message = __( 'You do not have access to this content.' );

        if ($this->request->is([ 'post' , 'ajax' ] ) )
        {
            $artist = $this->request->getData('artist');

            try
            {
                $query = $this->Artists->find('all')
                                ->where( ['OR' => [ ['twitter_handle LIKE ' => "%{$artist}%" ] , 'name LIKE ' => "%{$artist}%"  ] ] )
                                        ->order( [ 'name' => 'ASC' ] )
                                            ->group( [ 'name' ] )
                                                ->limit(9999);

                $data   = $this->paginate( $query )->toArray();

                if( !empty( $data ) )
                {
                    $message = __("Artist found." );
                    $status  = true;
                }
                else
                    $message = __("Artist not found." );

            }catch(NotFoundException $e){
                $message = __("Artist not found." );
            }
        }

        $response = $this->response($status , $data , $message );

        $this->set(compact('response'));
        $this->set('_serialize', 'response');
    }

    /**
     * Edit method
     *
     * @param string|null $id Artist id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $this->breadcrumbs[ __('Artists' ) ] = ['controller' => 'Artists' , 'action' => 'index' ];

        $artist = $this->Artists->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $artist = $this->Artists->patchEntity($artist, $this->request->getData());
            if ($this->Artists->save($artist)) {
                $this->Flash->success(__('The artist has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The artist could not be saved. Please, try again.'));
        }
        $users = $this->Artists->Users->find('list', ['limit' => 200]);
        $this->set(compact('artist', 'users'));
    }
}
