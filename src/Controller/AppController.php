<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Core\Configure;
use Cake\Event\Event;
use Cake\I18n\I18n;
use Cake\Mailer\Email;
use Cake\ORM\TableRegistry;

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link https://book.cakephp.org/3.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller
{

    protected $uses = [];

    public $appName = 'Musics';

    public $paginate = [
        'page' => 1,
        'limit' => 2,
        'maxLimit' => 100,
        'conditions' => ['active' => 'true']
    ];

    public $breadcrumbs;


    public $responser = null;

    public function initialize()
    {
        parent::initialize();

        // SET DEBUG
        if( $this->request->hasHeader('DEBUG') )
        {
            $debug =  strtolower( $this->request->getHeader('DEBUG')[0] ) == 'true' ? true : false ;
            Configure::write('debug' , $debug );
        }

        $this->loadComponent('RequestHandler', [
            'enableBeforeRedirect' => false
        ]);
        $this->loadComponent('Flash');

        if( $this->isAppCall() )
        {

            $this->loadComponent('Auth', [
                'authenticate' => [
                    'Basic' => [
                        'fields' => ['username' => 'username' , 'password' => 'api_key' , 'status' => 'true'],
                        'userModel' => 'Users'
                    ],
                    'Form' => ['username' , 'password' , 'status' => 'true'],
                ],
                'storage' => 'Memory',
                'unauthorizedRedirect' => true,
                'logoutRedirect' => [ 'controller' => 'users' , 'action' => 'login' ]
            ]);
        }
        else
        {
            $this->loadComponent('Auth', [
                'authenticate' => [
                    'Form' => ['username' , 'password' , 'status' => 'true'],
                ],
                'storage' => 'Session',
                'unauthorizedRedirect' => $this->referer()
            ]);
        }

        $this->Auth->setConfig('authError', "Sorry, we couldn't find an account with this username. Please check you're using the right username and try again.");
        
        $this->set('appName' , $this->appName);
        $this->set('paginate' , $this->paginate);

        // SET SIDEBAR
        $this->setSidebar();
    }


    private function setSidebar()
    {
        if( !$this->isAppCall() )
        {
            $this->breadcrumbs = [ 'Dashboard' => ['controller' => 'Users', 'action' => 'dashboard'] ];

            $sidebarMenu = [
                [ 'title' => __( 'Dashboard' ) , 'icon' => 'icon-speedometer' , 'href' => [ 'controller' => 'users' , 'action' => 'dashboard' ] ] ,
                [ 'title' => __( 'Artists' ), 'icon' => 'fa fa-users' , 'href' => [ 'controller' => 'artists' , 'action' => 'index' ]]    ,
                [ 'title' => __( 'Albums' ) , 'icon' => 'fa fa-book' , 'href' => [ 'controller' => 'albums' , 'action' => 'index' ]],
                //[ 'title' => __( 'Usuários' ) , 'icon' => 'fa fa-user' , 'href' => '/users']     ,

            ];

            $this->set( compact( 'sidebarMenu' ) );
        }
    }

    /**
     * Before render callback.
     *
     * @param \Cake\Event\Event $event The beforeRender event.
     * @return \Cake\Http\Response|null|void
     */
    public function beforeRender(Event $event)
    {
        if (!array_key_exists('_serialize', $this->viewVars) &&
            in_array($this->response->getType(), ['application/json', 'application/xml'])
        ) {
            $this->set('_serialize', true);
        }

        $this->set('breadcrumbs' , $this->breadcrumbs );
    }

    /**
     * Método que verifica se é uma chamada feita pelos Aplicativos
     * @return bool
     */
    protected function isAppCall()
    {
        $isJson = strpos( $this->request->getRequestTarget() , ".json" ) !== false ;
        $isApp  = $this->request->hasHeader('APP');


        if( $isApp AND strtoupper( $this->request->getHeader('APP')[0] ) != strtoupper( $this->appName) )
            return false;

        return $isApp AND $isJson;
    }

    /**
     * Método criado para configurar uma estrutura padrão de retorno da API
     * @param bool $status
     * @param null $data
     * @param null $message
     * @param null $controller [ Passado para definir em qual model será feita a paginação ]
     * @param string $version
     * @return mixed
     */
    public function response($status= false , $data = null , $message = null , $controller = null , $paginate = null ,  $version = '1.0')
    {

        $paginateItems = ['page' , 'current' , 'perPage' , 'pageCount' , 'prevPage' , 'nextPage'];

        if( empty( $controller) )
            $controller = $this->request->getParam('controller');

        if( !empty( $this->request->getParam('paging')[ $controller ] ) )
        {
            foreach ( $this->request->getParam('paging')[ $controller ] as $paginates => $p )
            {
                if( in_array( $paginates , $paginateItems ) )
                    $paginate[$paginates] = $p;
            }
        }

        $this->responser['status']     = $status;
        $this->responser['data']       = $data;
        $this->responser['paginate']   = $paginate;
        $this->responser['message']    = $message;
        $this->responser['version']    = $version;

        return $this->responser;
    }

    /**
     * Método criado para configurar retorno automático para serialização
     * @param bool $hasReturned
     */
    public function returned( $hasReturned = true )
    {
        if( $hasReturned )
        {
            $this->set(compact('response'));
            $this->set( '_serialize' , ['response'][0] );
        }
    }

    public function sendMail( $subject , $to , $message )
    {
        $configs = Configure::read('Email');

        $email = new Email( 'default' );

        return $email
            ->from([ '' => __( $this->appName )])
            ->emailFormat('html')
            ->to( $to )
            ->subject( __( $subject ) )
            ->send( __( $message ) );
    }
}
