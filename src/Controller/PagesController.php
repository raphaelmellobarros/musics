<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Aura\Intl\Exception;
use Cake\Core\Configure;
use Cake\Http\Exception\ForbiddenException;
use Cake\Http\Exception\NotFoundException;
use Cake\ORM\TableRegistry;
use Cake\View\Exception\MissingTemplateException;

/**
 * Static content controller
 *
 * This controller will render views from Template/Pages/
 *
 * @link https://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
class PagesController extends AppController
{

    /**
     * Displays a view
     *
     * @param array ...$path Path segments.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Http\Exception\ForbiddenException When a directory traversal attempt.
     * @throws \Cake\Http\Exception\NotFoundException When the view file could not
     *   be found or \Cake\View\Exception\MissingTemplateException in debug mode.
     */
    public function display(...$path)
    {
        $count = count($path);
        if (!$count) {
            return $this->redirect('/');
        }
        if (in_array('..', $path, true) || in_array('.', $path, true)) {
            throw new ForbiddenException();
        }
        $page = $subpage = null;

        if (!empty($path[0])) {
            $page = $path[0];
        }
        if (!empty($path[1])) {
            $subpage = $path[1];
        }
        $this->set(compact('page', 'subpage'));

        try {
            $this->render(implode('/', $path));
        } catch (MissingTemplateException $exception) {
            if (Configure::read('debug')) {
                throw $exception;
            }
            throw new NotFoundException();
        }
    }

    /**
     * Método para modificar um status de um registro "true" | "false"
     * @param model
     * @param status
     * @param id
     * @method POST
     */
    public function changeStatus()
    {
        $status  = false;
        $data    = null;
        $message = __( '' );


        if( $this->request->is( [ 'post' , 'ajax' ]) )
        {
            try
            {
                $models = [ 'Artists' , 'Albums'  , 'Users' ];

                $model       = $this->request->getData('model');
                $modelId     = $this->request->getData('id');
                $modelStatus = $this->request->getData('status');

                if( in_array( $model , $models ) )
                {
                    $models   = TableRegistry::get( $model );

                    $data = $models->find('all')
                        ->select( [ $model . '.id' , $model . '.status' ] )
                        ->where( [ $model . '.id' => $modelId ])->first();

                    if( !empty( $data ))
                    {
                        $data->status = $modelStatus;
                        $save = $models->save( $data );

                        if( $save )
                        {
                            $status  = true;
                            $message = __( 'Registro atualizado com sucesso' );
                        }
                        else
                            $message = __('Não foi possível atualizar o registro solicitado.');
                    }
                    else
                        $message = __('Não foi possível localizar o registro solicitado.');
                }
                else
                    $message = __('Não foi possível atualizar o registro solicitado.');
            }
            catch( Exception $e )
            {
                $message = __( 'Sua requisição não pôde ser completada. Tente novamente.' );
            }
        }

        $response = $this->response( $status , $data , $message );

        $this->set(compact('response' ));
        $this->set('_serialize', 'response');
    }
}
