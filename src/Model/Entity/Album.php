<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Album Entity
 *
 * @property int $id
 * @property int $artist_id
 * @property int $user_id
 * @property string $name
 * @property int $year
 * @property string $description
 * @property string $active
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\Artist $artist
 * @property \App\Model\Entity\User $user
 */
class Album extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'artist_id' => true,
        'user_id' => true,
        'name' => true,
        'year' => true,
        'description' => true,
        'active' => true,
        'created' => true,
        'modified' => true,
        'artist' => true,
        'user' => true
    ];
}
