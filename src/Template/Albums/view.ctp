<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Album $album
 */
?>
    <div class="row">
        <div class="col">
            <h3><?= __('View album') ?></h3>
        </div>
        <div class="w-100">&nbsp;</div>
    </div>

<?= $this->Form->create($album , [ 'url' => '#' , 'id' => 'addAlbum' ]) ?>
    <div class="row">
        <div class="col-lg col-md col-xl">
            <div class="w-100">&nbsp;</div>

            <?= $this->Form->control( 'artist_id' , [ 'type' => 'hidden' , 'id' => 'artist_id' , 'label' => false , 'class' => 'form-control' ] ); ?>
            <?= $this->Form->control( 'artist' , [ 'type' => 'text' , 'value' => $album->artist->name , 'id' => 'artist' , 'require' , 'disabled' , 'readonly' ,'placeholder' => __('Artist')  , 'label' => false , 'class' => 'form-control' ] ); ?>
            <div class="w-100">&nbsp;</div>

            <?= $this->Form->control( 'name' , [ 'type' => 'text' , 'id' => 'name' , 'require' , 'disabled' , 'readonly' ,'placeholder' => __('Album name')  , 'label' => false , 'class' => 'form-control' ] ); ?>
            <div class="w-100">&nbsp;</div>

            <?= $this->Form->control( 'year' , [ 'type' => 'text' , 'id' => 'year' , 'require' , 'disabled' , 'readonly' ,'placeholder' => __('Year')  , 'label' => false , 'class' => 'year form-control' ] ); ?>
            <div class="w-100">&nbsp;</div>

            <?= $this->Form->control( 'description' , [ 'type' => 'textarea' , 'id' => 'description' , 'require' , 'disabled' , 'readonly' ,'placeholder' => __('Album descriptions')  , 'label' => false , 'class' => 'form-control' ] ); ?>
            <div class="w-100">&nbsp;</div>

            <div class="w-100"><?= _('Published?') ;?></div>
            <?= $this->Form->control( 'active' , [ 'type' => 'select' , 'options' => [ 'true' => 'yes' , 'false' => 'no' ] , 'id' => 'active' , 'require' , 'disabled' , 'readonly' ,'placeholder' => __('Published?')  , 'label' => false , 'class' => 'form-control' ] ); ?>
            <div class="w-100">&nbsp;</div>
        </div>
    </div>
<?= $this->Form->end() ?>


<?= $this->Html->script('/vendors/jquery-ui-1.12.1/jquery-ui.min'); ?>
<?= $this->Html->css('/vendors/jquery-ui-1.12.1/jquery-ui.min'); ?>

<?= $this->element( 'Javascript/albums' , [ "cache" => false ] ); ?>