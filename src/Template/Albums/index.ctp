<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Album[]|\Cake\Collection\CollectionInterface $albums
 */
?>
<div class="row">
    <div class="col">
        <h3><?= __('Albums') ?></h3>
    </div>
    <div class="w-100">&nbsp;</div>
    <div class="col">
        <?php if( !empty( $artists )  ) : ?>
            <?= $this->Html->link( __( '{0} Add album' , [ '<i class="fa fa-plus"></i>' ] ) , [ 'controller' => 'Albums' , 'action' => 'add' ] , [ 'escape' => false , 'class' => 'btn btn-success pull-right' ]  ) ;?>
        <?php endif; ?>
        <?php if( empty( $artists )  ) : ?>
            <?= $this->Html->link( __( '{0} Insert one artist' , [ '<i class="fa fa-plus"></i>' ] ) , [ 'controller' => 'Artists' , 'action' => 'add' ] , [ 'escape' => false , 'class' => 'btn btn-warning pull-right' ]  ) ;?>
        <?php endif; ?>
    </div>
    <div class="w-100">&nbsp;</div>
</div>

<div class="row">
    <?php if( count( $albums ) > 0 ): ?>
    <div class="table-responsive">
        <table class="table table-hover ">
            <thead>
            <tr>
                <th scope="col" class="text-center">#</th>
                <th scope="col"><?= $this->Paginator->sort('artist_id' , __('Artist')) ?></th>
                <th scope="col"><?= $this->Paginator->sort('user_id' , __('User')) ?></th>
                <th scope="col"><?= $this->Paginator->sort('name' , __('Album') ) ?></th>
                <th scope="col"><?= $this->Paginator->sort('year' , __('Year')) ?></th>
                <th scope="col" class="col-status"><?= $this->Paginator->sort('active' , __('Published') ) ?></th>
                <th scope="col" class="actions text-right"><?= __('') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($albums as $k => $album): ?>
            <tr>
                <th scope="row" class="text-center"><?= $this->Number->format( $k + 1 ) ?></th>
                <td><?= $album->has('artist') ? $this->Html->link($album->artist->name, ['controller' => 'Artists', 'action' => 'view', $album->artist->id]) : '' ?></td>
                <td><?= $album->has('user') ? $album->user->username : '' ?></td>
                <td><?= h($album->name) ?></td>
                <td><?= $album->year ?></td>
                <td class="col-status">
                    <?= $this->element('Actions/active' , [ 'model' => 'albums' , 'modelId' => $album->id , 'modelStatus' => $album->active ] , ['cache' => false ]) ;?>
                </td>
                <td class="actions">
                    <?= $this->element('Actions/actions' , [ 'model' => 'albums' ,  'modelId' => $album->id ] , ['cache' => false ]) ;?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
        </table>
    </div>
    <?php endif ; ?>

    <?php if( count( $albums ) <= 0 ): ?>
        <div class="col-8 offset-2 text-center alert alert-danger"><?= __('Não existem álbuns cadastrados no momento.') ;?></div>
    <?php endif ; ?>


</div>

<?php
    if( count( $albums ) > $paginate['limit'] )
        echo $this->element('Navigation/pagination') ;
    else
        //TODO MOSTRAR MENSAGEM QUE NÂO EXISTE ARTISDTA CADASTRADO
?>
