<?php
use Cake\Core\Configure;
use Cake\Error\Debugger;

$this->layout = 'error';

if (Configure::read('debug')) :
    $this->layout = 'dev_error';

    $this->assign('title', $message);
    $this->assign('templateName', 'error400.ctp');

    $this->start('file');
?>
<?php if (!empty($error->queryString)) : ?>
    <p class="notice">
        <strong>SQL Query: </strong>
        <?= h($error->queryString) ?>
    </p>
<?php endif; ?>
<?php if (!empty($error->params)) : ?>
        <strong>SQL Query Params: </strong>
        <?php Debugger::dump($error->params) ?>
<?php endif; ?>
<?= $this->element('auto_table_warning') ?>
<?php
if (extension_loaded('xdebug')) :
    xdebug_print_function_stack();
endif;

$this->end();
endif;
?>


<div class="row justify-content-center">
    <div class="col-md-8">
        <div class="card-group">
            <div class="card p-4" style="background:transparent;border:0px;">
                <div class="card-body text-center">
                    <div class="col-12" style="font-size:10vw;">404</div>
                    <div class="col-12" style="font-size:1.3vw;">
                        <?= __('The address {0} was not found' , "<strong>'{$url}'</strong>") ?>
                    </div>
                    <div class="col-12">
                        <br>
                        <?= $this->Html->link( __('{0} Back' , '<i class="fa fa-backward"></i>' )  , ['controller' => 'users' , 'action' => 'dashboard' ] , [ 'class' => 'btn btn-primary col-2' , 'escape' => false ] ) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
