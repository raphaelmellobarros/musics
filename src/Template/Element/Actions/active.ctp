<?php if( !empty( $model ) && !empty( $modelId ) && !empty( $modelStatus ) ) : ?>
    <div class="elements text-center">

        <?php
        $classBtn  = $modelStatus == 'true' ? 'btn-success' : 'btn-danger';
        $classIcon = $modelStatus == 'true' ? 'fa-check-circle-o' : 'fa-times-circle-o';

        echo $this->Html->link("<i class='fa {$classIcon}'></i>"  , '#' , [ 'style' => 'cursor:default;' , 'class' => "btn {$classBtn}" , 'escape' => false] );

        /*
        <label class="switch switch-text switch-success">
        <input type="checkbox" model="<?= $model ?>" modelid="<?= $modelId ?>" class="switch-input" <?= $modelStatus == 'true' ? 'checked=""' : '' ?>">
        <span class="switch-label" data-on="On" data-off="Off"></span>
        <span class="switch-handle"></span>
        </label>
         */

        ?>
    </div>
<?php endif; ?>