<div class="elements text-right">
    <?php if( !empty( $modelId ) AND !empty( $model ) ) : ?>
        <?= $this->Html->link( '' , [ 'controller' => $model , 'action' => 'view' , $modelId ] ,[ 'class' => 'fa fa-eye btn btn-info tableView' ] )         ?>
        <?= $this->Html->link( '' , [ 'controller' => $model , 'action' => 'edit' , $modelId ] ,[ 'class' => 'fa fa-edit btn btn-warning tableEdit' ] )         ?>
    <?php endif; ?>
</div>
