<div class="elements text-center">
    <?php if( !empty( $modelImage )) : ?>
        <?php
            $images = explode("," , $modelImage);

            foreach($images as $image)
                echo $this->Html->image("categories/{$image}" , [ 'class' => 'btn table-image-preview']);
        ?>
    <?php endif; ?>
</div>
