<?php if( !empty( $url ) ): ?>
    <?= $this->Html->css('elements/form_searchbar') ?>
    <?= $this->Html->script('elements/form_searchbar') ?>

    <div>
        <?= $this->Form->create( 'seacrh' , [ 'id' => 'search' , 'class' => 'searchbox' , 'url' => $url] ) ?>
            <?php $this->Form->templates( [ 'inputContainer' => '{{content}}' ] ); ?>
            <div class="form-group">
                <?= $this->Form->control('search' , [ 'id' => 'search' , 'class' => 'searchbox-input form-control' , 'required' , 'placeholder' => __('Pesquisar...' ) , 'label' => false ]); ?>
                <!-- <input type="submit" class="searchbox-submit" value=""> -->
                <span class="searchbox-submit"><i class="icon icon-magnifier"></i></span>
                <span class="searchbox-icon"><i class="icon icon-magnifier"></i></span>
            </div>
        <?= $this->Form->end() ?>
    </div>

<?php endif; ;?>