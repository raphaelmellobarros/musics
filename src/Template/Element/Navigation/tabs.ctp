<?php if( !empty( $modelName ) ): ?>
    <ul class="nav nav-tabs" role="tablist">
        <li class="nav-item">
            <a class="nav-link active" data-toggle="tab" href="#home3" role="tab" aria-controls="home"><i class="fa fa-th"></i> <?= __( $modelName ) ?></a>
        </li>
        <li class="nav-item">
            <a class="nav-link tabAdd" data-toggle="tab" href="#profile3" role="tab" aria-controls="profile"><i class="fa fa-plus"></i> <?= __('Adicionar/Editar') ?></a>
        </li>
        <!--
        <li class="nav-item">
            <a class="nav-link" data-toggle="tab" href="#messages3" role="tab" aria-controls="messages"><i class="fa fa-bar-chart"></i>  <?= __('Gráficos') ?></a>
        </li>
        -->
    </ul>
<?php endif ; ?>