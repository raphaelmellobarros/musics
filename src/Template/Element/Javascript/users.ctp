<script type="text/javascript">
    $(function()
    {
        // function for login
        function doLogin( form )
        {
            var data  = form.serializeArray();
            var error = true;

            $.each( data , function( k , v )
            {
                if( v.name != '_method' )
                {
                    //console.log(attr);

                    if( v.value.length < 3)
                    {
                        error = true;
                        var msg = "<?= __("Sorry, we couldn't find an account with this username. Please check you're using the right username and try again."); ?>";
                        toast.text = msg;
                        toast.icon = 'error';

                        $.toast( toast );
                        return false;
                    }
                    else
                        error = false;
                }
            });

            if( !error )
            {
                var send = sendForm( form  , form.attr( 'action' ) , false );

                if ( send.status )
                    window.location = '<?= \Cake\Routing\Router::url ([ 'controller'  =>  'users' , 'action' => 'dashboard' ]) ?>';
            }
        }

        $( '#username' ).on( 'keypress' , function( e )
        {
            if( e.which == 13 )
                $( '#password' ).focus();
        });

        $( '#password' ).on( 'keypress' , function( e )
        {
            if( e.which == 13 )
            {
                var form  = $( this ).parents( 'form' );
                doLogin( form ) ;
            }
        });

        $( '#sendLogin' ).on( 'click' , function()
        {
            var form  = $( this ).parents( 'form' );
            doLogin( form );
        })
    });
</script>