<script type="text/javascript">
    jQuery(function()
    {
        function searchArtist( artist )
        {
            jQuery.ajax({
                url : '<?= \Cake\Routing\Router::url ([ 'controller'  =>  'artists' , 'action' => 'search.json' ]) ?>',
                data : { 'artist' : artist },
                dataType : 'json',
                type : 'POST',
                success : function( r )
                {
                    if( !isLogged() )
                        location.reload();
                    else{
                        if( r.status )
                        {
                            var source = new Array();
                            jQuery.each( r.data , function(k,v){
                                source.push( { "label" : v.name , "value" : v.name  , "id" : v.id.toString() } );
                            });


                            jQuery('#artist').autocomplete({
                                source : source  ,
                                select : function(e, ui){
                                    jQuery('#artist_id').val( ui.item.id );
                                    jQuery('#artist').removeClass('border border-danger').addClass('border border-success');
                                }
                            });
                        }else{
                            artist = '';
                            jQuery('#artist').val( artist ).removeClass('border border-success').addClass('border border-danger');
                            jQuery('#artist_id').val( '' );
                        }
                    }
                },
                error : function( r ){
                    if( !isLogged() )
                        location.reload();
                    else {
                        artist = '';
                        jQuery('#artist').val(artist).removeClass('border border-success').addClass('border border-danger');
                        jQuery('#artist_id').val('');
                    }
                }
            });
        }


        jQuery( '.year' ).mask('9999');

        if( jQuery('#artist').val().length >= 2 )
            searchArtist(jQuery('#artist').val());

        jQuery('#artist').on( 'keyup' , function( e )
        {
            var artist = jQuery( this ).val();

            jQuery('#artist').removeClass('border border-success').addClass('border border-danger');

            if( artist.length >= 2 )
                searchArtist( jQuery( this ).val() )
        });

    });
</script>
