<?php
    header('Cache-Control: no-store, private, no-cache, must-revalidate');                  // HTTP/1.1
    header('Cache-Control: pre-check=0, post-check=0, max-age=0, max-stale = 0', false);    // HTTP/1.1
    header('Pragma: public');
    header('Expires: Sat, 26 Jul 1997 05:00:00 GMT');                                       // Date in the past
    header('Expires: 0', false);
    header('Last-Modified: '.gmdate('D, d M Y H:i:s') . ' GMT');
    header('Pragma: no-cache');
?>

<!DOCTYPE html>
<html lang="pt-BR">
<head>
    <?= $this->Html->charset() ?>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="<?= $appName ?>">
    <meta name="author" content="raphaeldemellobarros">
    <meta name="keyword" content="music,artists,">
    <title>
        <?= !empty( $appName ) ? __( $appName ) . ' :' : ' ' ?>
        <?= __( $this->fetch('title') ) ?>
    </title>
    <?= $this->Html->meta('icon') ?>

    <?php // VENDORS // ?>
    <?=
        $this->Html->css(
            [
                '/vendors/font-awesome/css/font-awesome.min' ,
                '/vendors/simple-line-icons/css/simple-line-icons',
                '/vendors/jquery-toast-plugin/jquery.toast.min',
                '/vendors/jquery-tag-input/jquery.tagsinput.min'
                //'/vendors/materialdesign/material-components-web/dist/material-components-web'
            ]
        )
    ?>

    <?php // Main styles for this application // ?>
    <?php // Styles required by this views // ?>
    <?= $this->Html->css([ 'style.min' ]) ?>

    <!-- Bootstrap and necessary plugins -->
    <?=
        $this->Html->script(
            [
                '/vendors/jquery/dist/jquery.min' ,
                '/vendors/popper.js/dist/umd/popper.min' ,
                '/vendors/bootstrap/dist/js/bootstrap.min' ,
                '/vendors/pace-progress/pace.min' ,
                '/vendors/jquery-toast-plugin/jquery.toast.min',
                '/vendors/jquery-tag-input/jquery.tagsinput.min',
                '/vendors/jquery-mask-plugin/jquery.mask'
            ]
        )
    ?>

    <!-- Plugins and scripts required by all views -->
    <?= $this->Html->script([ '/vendors/chart.js/dist/Chart.min']) ?>

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>

</head>

<body class="app header-fixed sidebar-fixed aside-menu-fixed aside-menu-hidden">

<?= $this->element('Modals/primary_modal' , [ 'id' => 'primaryModal' , 'title' => 'modal' , 'body' => 'modal'] ); ?>

<header class="app-header navbar">
    <button class="navbar-toggler mobile-sidebar-toggler d-lg-none mr-auto" type="button">
        <span class="navbar-toggler-icon"></span>
    </button>
    <a class="navbar-brand" href="#"></a>
    <button class="navbar-toggler sidebar-toggler d-md-down-none" type="button">
        <span class="navbar-toggler-icon"></span>
    </button>

    <ul class="nav navbar-nav ml-auto">
        <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                <?= $this->Html->image('avatar.png' , ['class' => 'img-avatar' , 'alt' => $appName]); ?>
            </a>
            <div class="dropdown-menu dropdown-menu-right">
                <div class="dropdown-header text-center">
                    <strong><?= __( 'Settings' ); ?></strong>
                </div>
                <?= $this->Html->link("<i class=\"fa fa-lock\"></i> " . __( 'Logout' ) , [ 'controller' => 'users' , 'action' => 'logout' ]  , [ 'class' => 'dropdown-item' , 'escape' => false ] ); ?>
            </div>
        </li>
    </ul>
</header>

<div class="app-body">
    <div class="sidebar">
        <nav class="sidebar-nav">
            <ul class="nav">
                <?php foreach( $sidebarMenu as $sbm ): ?>
                    <li class="nav-item nav-dropdown">
                        <?= $this->Html->link( "<i class=\"{$sbm['icon']}\"></i> " . $sbm['title'] , $sbm['href'] , [ 'class' => 'nav-link nav-dropdown-toggle' , 'escape' => false ]); ?>
                    </li>
                <?php endforeach; ?>
            </ul>
        </nav>
        <button class="sidebar-minimizer brand-minimizer" type="button"></button>
    </div>

    <!-- Main content -->
    <main class="main">

        <!-- Breadcrumb -->
        <?php

            if( !empty( $breadcrumbs ) )
            {
                foreach( $breadcrumbs as $breadcrumb => $bread)
                {
                    $this->Breadcrumbs->add(
                        $breadcrumb,
                        ['controller' => $bread['controller'] , 'action' => $bread['action']]
                    );
                }
            }

            $this->Breadcrumbs->setTemplates(
                [
                    'wrapper' => '<ol{{attrs}} class="breadcrumb">{{content}}</ol>',
                    'item' => '<li{{attrs}} class="breadcrumb-item"><a href="{{url}}"{{innerAttrs}}>{{title}}</a></li>{{separator}}',
                    'itemWithoutLink' => '<li{{attrs}} class="breadcrumb-item"><span{{innerAttrs}}>{{title}}</span></li>{{separator}}',
                    //'separator' => '<li{{attrs}}><span{{innerAttrs}}>{{separator}}</span></li>'
                ]
            );

            echo $this->Breadcrumbs->render();
        ?>

        <?php if( $this->Flash->render('danger') ) : ?>
            <div class="container-fluid">
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    <?= $this->Flash->render('danger') ?>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Fechar">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </div>
        <?php endif; ?>

        <?php if( $this->Flash->render('success') ) : ?>
            <div class="container-fluid">
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    <?= $this->Flash->render('success') ?>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Fechar">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </div>
        <?php endif; ?>

        <div class="container-fluid">
            <div class="animated fadeIn">
                <?= $this->fetch('content') ?>
            </div>
        </div>
        <!-- /.container-fluid -->
    </main>
</div>

<footer class="app-footer">
    <span><a href="http://apiservices.com.br" target="_blank">APISERVICES</a> © <?= date('Y') ?></span>
</footer>

<!-- CoreUI main scripts -->
<?= $this->Html->script([ 'tables' ]) ?>

<?= $this->element( 'Javascript/app' , [ "cache" => false ] ); ?>

</body>
</html>