<!DOCTYPE html>
<html lang="pt-BR" >
<head>
    <?= $this->Html->charset() ?>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="<?= $appName ?>">
    <meta name="author" content="raphaeldemellobarros">
    <meta name="keyword" content="music,musica,artists,artistas,albums">

    <title>
        <?= !empty( $appName ) ? $appName . ' :' : ' ' ?>
        <?= $this->fetch('title') ?>
    </title>
    <?= $this->Html->meta('icon') ?>

    <?php // VENDORS // ?>
    <?=
    $this->Html->css(
        [
            '/vendors/font-awesome/css/font-awesome.min' ,
            '/vendors/simple-line-icons/css/simple-line-icons',
            '/vendors/jquery-toast-plugin/jquery.toast.min',
            '/vendors/jquery-tag-input/jquery.tagsinput.min'
            //'/vendors/materialdesign/material-components-web/dist/material-components-web'
        ]
    )
    ?>

    <?php // Main styles for this application // ?>
    <?php // Styles required by this views // ?>
    <?= $this->Html->css([ 'style.min' ]) ?>

    <!-- Bootstrap and necessary plugins -->
    <?=
    $this->Html->script(
        [
            '/vendors/jquery/dist/jquery.min' ,
            '/vendors/bootstrap/dist/js/bootstrap.min'
        ]
    )
    ?>

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>

</head>

<body class="app flex-row align-items-center">
    <div class="container">
        <?= $this->fetch('content') ?>
    </div>
</body>

</html>