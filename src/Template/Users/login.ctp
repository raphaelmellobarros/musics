<div class="row justify-content-center">
    <div class="col-md-4">
        <div class="card-group">
            <div class="card p-4">
                <div class="card-body">
                    <h1><?= __( 'Login') ?></h1>
                    <p class="text-muted"><?= __( 'Use your credentials here!') ?></p>

                    <?= $this->Form->create( $user , [ 'id' => 'userLogin' , 'url' => '/login.json' ] ) ?>
                        <?php $this->Form->setTemplates( [ 'inputContainer' => '{{content}}' ] ); ?>
                        <div class="input-group mb-3">
                            <span class="input-group-addon"><i class="icon-user"></i></span>
                            <?= $this->Form->control('username' , [ 'id' => 'username' , 'class' => 'form-control' , 'placeholder' => __('Username' ) , 'label' => false ]); ?>
                        </div>
                        <div class="input-group mb-4">
                            <span class="input-group-addon"><i class="icon-lock"></i></span>
                            <?= $this->Form->control('password' , [ 'id' => 'password' , 'class' => 'form-control' , 'placeholder' => __('Password' ) , 'label' => false ]); ?>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <?= $this->Form->button( __('<i class="fa fa-send"></i> ' . __('Send') ) , ['id' => 'sendLogin' ,  'type' => 'button' , 'class' => 'btn pull-right btn-primary px-4 sendform'] , ['escape' => false]) ?>
                            </div>
                        </div>
                    <?= $this->Form->end() ?>
                </div>
            </div>
        </div>
    </div>
</div>

<?= $this->element( 'Javascript/users' , [ "cache" => false ] ); ?>