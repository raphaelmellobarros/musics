<div class="row">

    <?php if( !empty( $dashboards ) ) : ?>
        <?php foreach( $dashboards as $dashboard ): ?>
                <div class="col-sm-6 col-md-2">
                    <div class="card text-white <?= $dashboard['color'] ?>">
                        <div class="card-body">
                            <div class="h1 text-muted text-right mb-4">
                                <i class="<?= $dashboard['icon'] ?>"></i>
                            </div>
                            <div class="h4 mb-0"><?= $dashboard['count'] ?></div>
                            <small class="text-muted text-uppercase font-weight-bold"><?= $dashboard['title'] ?></small>
                            <!--
                            <div class="progress progress-white progress-xs mt-3">
                                <div class="progress-bar" role="progressbar" style="width: 25%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                            -->
                        </div>
                    </div>
                </div>
        <?php endforeach; ?>

    <?php endif ?>

</div>