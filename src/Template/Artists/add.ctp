<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Artist $artist
 */
?>
<div class="row">
    <div class="col">
        <h3><?= __('Add artist') ?></h3>
    </div>
    <div class="w-100">&nbsp;</div>
</div>

<?= $this->Form->create($artist , [ 'id' => 'addArtist' ]) ?>
<div class="row">
    <div class="col-lg col-md col-xl">
        <div class="w-100">&nbsp;</div>

        <?= $this->Form->control( 'name' , [ 'type' => 'text' , 'id' => 'name' , 'require' , 'placeholder' => __('Artist name')  , 'label' => false , 'class' => 'form-control' ] ); ?>
        <div class="w-100">&nbsp;</div>

        <?= $this->Form->control( 'twitter_handle' , [ 'type' => 'text' , 'id' => 'twitter_handle' , 'require' , 'placeholder' => __('Twitter handle')  , 'label' => false , 'class' => 'form-control' ] ); ?>
        <div class="w-100">&nbsp;</div>

        <div class="w-100"><?= _('Published?') ;?></div>
        <?= $this->Form->control( 'active' , [ 'type' => 'select' , 'options' => [ 'true' => 'yes' , 'false' => 'no' ] , 'id' => 'active' , 'require' , 'placeholder' => __('Published?')  , 'label' => false , 'class' => 'form-control' ] ); ?>
        <div class="w-100">&nbsp;</div>

        <?= $this->Form->button( __( '{0} save' , [ '<i class="fa fa-plus"></i>' ] ) , [  'id' => 'save' , 'type' => 'submit' , 'label' => false , 'class' => 'btn btn-success float-md-right float-lg-right col-lg-3 col-md-3 col-sm-12' ] ); ?>
        <div class="w-100">&nbsp;</div>
    </div>
</div>
<?= $this->Form->end() ?>
