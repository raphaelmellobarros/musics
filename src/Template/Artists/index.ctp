<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Artist[]|\Cake\Collection\CollectionInterface $artists
 */
?>
<div class="row">
    <div class="col">
        <h3><?= __('Artists') ?></h3>
    </div>
    <div class="w-100">&nbsp;</div>
    <div class="col">
        <?= $this->Html->link( __( '{0} Add Artist' , [ '<i class="fa fa-plus"></i>' ] ) , [ 'controller' => 'Artists' , 'action' => 'add' ] , [ 'escape' => false , 'class' => 'btn btn-success pull-right' ]  ) ;?>
    </div>
    <div class="w-100">&nbsp;</div>
</div>

<div class="row">
    <?php if( count( $artists ) > 0 ): ?>
    <div class="table-responsive">
        <table class="table table-hover ">
            <thead>
            <tr>
                <th scope="col" class="text-center">#</th>
                <th scope="col"><?= $this->Paginator->sort('user_id' , __('User') ) ?></th>
                <th scope="col"><?= $this->Paginator->sort('name' , __('Artist') ) ?></th>
                <th scope="col"><?= $this->Paginator->sort('twitter_handle' , __('Twitter')) ?></th>
                <th scope="col" class="col-status"><?= $this->Paginator->sort('active' , __('Published') ) ?></th>
                <th scope="col" class="actions text-right"><?=  __('') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($artists as $k => $artist): ?>
            <tr>
                <th scope="row" class="text-center"><?= $this->Number->format( $k + 1 ) ?></th>

                <td><?= $artist->has('user') ? $artist->user->username : '' ?></td>
                <td><?= h($artist->name) ?></td>
                <td><?= $this->Html->link( $artist->twitter_handle , 'https://twitter.com/' . strtolower( str_replace( '@' , '' , $artist->twitter_handle) ) , [ 'target' => '_blank'] ) ?></td>
                <td class="col-status">
                    <?= $this->element('Actions/active' , [ 'model' => 'artists' , 'modelId' => $artist->id , 'modelStatus' => $artist->active ] , ['cache' => false ]) ;?>
                </td>
                <td class="actions">
                    <?= $this->element('Actions/actions' , [ 'model' => 'artists' ,  'modelId' => $artist->id ] , ['cache' => false ]) ;?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
        </table>
    </div>
    <?php endif ; ?>

    <?php if( count( $artists ) <= 0 ): ?>
        <div class="col-8 offset-2 text-center alert alert-danger"><?= __('There are no artists currently registered.') ;?></div>
    <?php endif ; ?>


</div>

<?php
    if( count( $artists ) > $paginate['limit'] )
        echo $this->element('Navigation/pagination') ;
    else
        //TODO MOSTRAR MENSAGEM QUE NÂO EXISTE ARTISDTA CADASTRADO
?>
