<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Artist $artist
 */
?>
<div class="row">
    <div class="col">
        <h3><?= __('View artist') ?></h3>
    </div>
    <div class="w-100">&nbsp;</div>
</div>

<?= $this->Form->create($artist , [  'url' => '#' , 'id' => 'addArtist' ]) ?>
    <div class="row">
        <div class="col-lg col-md col-xl">
            <div class="w-100">&nbsp;</div>

            <?= $this->Form->control( 'name' , [ 'type' => 'text' , 'id' => 'name' , 'require' , 'placeholder' => __('Artist name')  , 'readonly', 'disabled' ,  'label' => false , 'class' => 'form-control' ] ); ?>
            <div class="w-100">&nbsp;</div>

            <?= $this->Form->control( 'twitter_handle' , [ 'type' => 'text' , 'id' => 'twitter_handle' , 'require' , 'placeholder' => __('Twitter andle')  ,  'readonly', 'disabled' , 'label' => false , 'class' => 'form-control' ] ); ?>
            <div class="w-100">&nbsp;</div>

            <div class="w-100"><?= _('Published?') ;?></div>
            <?= $this->Form->control( 'active' , [ 'type' => 'select' , 'options' => [ 'true' => 'yes' , 'false' => 'no' ] , 'id' => 'active' , 'require' ,  'readonly', 'disabled' ,  'placeholder' => __('Published?')  , 'label' => false , 'class' => 'form-control' ] ); ?>
            <div class="w-100">&nbsp;</div>
        </div>
    </div>
<?= $this->Form->end() ?>
